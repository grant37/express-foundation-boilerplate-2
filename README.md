# Express + Foundation Boilerplate

To use this boilerplate, I have included `bp.js` which is a node-cli app 
specifically designed for this boilerplate.  

This app will let you:

  * Install all required npm dependencies and gems for this boilerplate
  * Build the folder structure of the boilerplate and install package deps / bower comp
  * Watch & compile changes in the public/scss directory using either grunt or compass
  * Update Foundation's bower components
  * Start your app.js

## Initial Install
`bp.js install` Will install the inital dependencies we need for this boilerplate.

If you are using Grunt, this will remove `public/config.rb` and install:  

  * `npm install -g bower grunt-cli`
  * `gem install foundation`

If you are using Compass, this will remove `Gruntfile.js` and install:  
  
  * `npm install -g bower`
  * `gem install foundation`
  * `gem install compass`

The commands:

  * Using Grunt
    - Run `[sudo] node bp.js install true`
  * Using Compass
    - Run `[sudo] node bp.js install`

## Building the boilerplate
`bp.js build` Will build the boilerplate for inital use. This will install all node modules
and will install all the bower components for using Foundation. This will make sure
folder structure is set-up and will compile `app.scss`.

The commands:

  * Using Grunt
    - Run `[sudo] node bp.js build true`
  * Using Compass
    - Run `[sudo] node bp.js build`

### Watching
After being built. You can now start using the boilerplate. You can run 
`[sudo] node bp.js watch true` to let Grunt watch your scss folder for changes 
and compile the files, or you can omit the `true` parameter if you are using 
Compass.
 
### Starting 
You can run `[sudo] node bp.js start` as way to run `app.js`.